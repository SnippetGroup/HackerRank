#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    unsigned int n = 0;
    unsigned int q = 0;
    cin >> n >> q;
    
    int *a[n];
    
    for (unsigned int i = 0; i < n; ++i)
    {
        unsigned int k = 0;
        cin >> k;
        
        a[i] = new int[k];
        
        for (unsigned int j = 0; j < k; ++j)
        {
            cin >> a[i][j];
        }
    }
    
    for (unsigned int q_i = 0; q_i < q; ++q_i)
    {
        unsigned int i = 0;
        unsigned int j = 0;
        cin >> i >> j;
        
        cout << a[i][j] << endl;
    }
      
    return 0;
}
